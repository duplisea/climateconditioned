---
title: "Overview"
weight: 1
header_menu: true
---

Risk provides the underpinning for transparent and consistent decision making in marine resource management. Climate and ecosystem changes are now occurring on a rate rapid enough to affect the medium to long term viability of sustainable management decisions. It is no longer acceptable to provide advice for management of human activities in the marine environment assuming status quo background conditions in the environment when some of that advice may become invalid in short order. A risk-based approach to advice and management that utilises modern analytical tools appropriate to each data and knowledge situation, manages towards clear objectives and adopts risk equivalency is a pragmatic approach to marine management during times of rapid climate and ecosystem change.

![gs](images/Gulf.stream.webp)