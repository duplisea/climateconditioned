---
title: 'What are risk and risk-equivalence ?'
weight: 2
header_menu: true
---
The language of risk is vast, rich (see <https://simplicable.com/new/risk>) and often confusing. One of the main problems for understanding risk concepts in a particular field is that risk is a part of practically every human activity and therefore has multiple meanings that can differ in between fields. Risk is also a frequently used word in everyday speech to describe many different concepts around danger and reward. This means that people often feel that they understand risk, and they might, but they also likely have an understanding of risk that is aligned to their discipline or interests. We are no exception, but in order to clarify our interpretation, the commonly used terms here have been defined in the context of marine resource management and advice.

#### Risk

Risk is the chance that something undesired will happen. With objectives based fishery management, risk is usually stated as the probability that a stock biomass objective will not be met or the chance that a stock's biomass will fall below a poor health objective such as the limit reference point. Risk should be understood more generally than this though. For instance governments manage many different kinds of activities in the marine environment in addition to fisheries such as marine infrastructure, oil and gas development, shipping and tourism. Management of these activities is a form of risk management and in some cases this is clear while in others it is not easy to see what risk is being managed. In cases where it is not clear that the management decision is actually a risk based decision, it is often because there are no stated operational objectives that management is trying to achieve; therefore, the risk management is at best implicit. Implicit risk management is not good practice, however, because it is not often clear what risk is being managed, objectives and quantities may change without others knowing and this leads to *ad hoc* management that is not transparent.

#### Risk-based advice

Because all management decisions about natural resource exploitation are risk-based decisions, it is logical that science provides appropriate risk based advice and analyses to enable managers to make informed risk based decisions. Risk management involves the understanding of trade-offs between harm to resources and economic and social benefits that come from resource exploitation. Risk based advice therefore is most useful to decision makers when it is provided within the constraints of existing legislative and policy based risk consequence frameworks. The precautionary approach to fisheries is one such framework which lays out the risk consequence landscape for managing fish stocks in many jurisdictions. Because a clearly defined framework does not exist to orient risk-based advice and decisions for management of many marine activities, it is important that science provides a range of risk options with potential consequences of different management actions and highlight options which entail an equivalent risk consistent with previous management actions.

#### Risk equivalence

Risk equivalence is a term to denote management advice or an action that maintains a prescribed risk tolerance level across, stocks, years or when uncertainty in the evaluation changes. Thus risk equivalency can be broadly thought of as consistency in management. For example if a policy or manager allowed a 5% chance of falling below a limit reference point in the next year for a decision on directed fishery removal, then we can assume that this 5% risk tolerance is the same level of risk allowed if we considered the impact of a marine heat-wave mortality event on the stock. That is, a risk equivalent management action that would consider a marine heat wave mortality event would need to reduce directed fishery removals to compensate the mortality loss owing to the heat-wave thus maintaining the 5% risk level of stock decline below the limit reference point.

Dr. Marie-Julie Roux explains risk and risk-equivalency in advice and decision making in this 6 minute video that she presented at the ICES Annual Science Conference in September 2021.

{{< youtube id="5OfJcUDkj1c" >}}

She [published a paper](https://doi.org/10.3389/fclim.2021.781559) in 2022 further explaining these concepts.

![arcticarchaelogy](images/nuno-antunes-R0gWpkBBlvY-unsplash.webp)