---
title: "Publications"
weight: 7
header_menu: true
---

Several publications have been produced in recent years describing how to incorporate climate and ecosystem changes in vulnerability assessment and advice. These are a few we have been involved with:
&nbsp;

•*Bahri, T., Vasconcellos, M., Welch, DJ, Johnson, J, Perry, RI, Ma, X, & Sharma, R (editors). 2021. Adaptive management of fisheries in response to climate change. FAO Fisheries and Aquaculture Technical Paper No. 667. http://www.fao.org/documents/card/en/c/cb3095en/*

•*Duplisea, DE, Roux, MJ, Hunter, KL and Rice, J., 2021. Fish harvesting advice under climate change: A risk-equivalent empirical approach. PloS one, 16(2), p.e0239503. https://journals.plos.org/plosone/article/authors?id=10.1371/journal.pone.0239503*

•*Duplisea, DE, Roux, M-J, Hunter, KL, and Rice, J. 2020. Resource management under climate change: a risk-based strategy to develop climate-informed science advice. DFO Can. Sci. Advis. Sec. Res. Doc. 2019/044. v + 45 p. https://waves-vagues.dfo-mpo.gc.ca/Library/40874126.pdf*

•*Hunter, KL, Wade, J, Stortini, CH, Hyatt, KD, Christian, JR, Pepin, P, Pearsall, IA, Nelson, MW, Perry, RI and Shackell, NL. 2015. Climate Change Vulnerability Assessment Methodology Workshop Proceedings. Can. Manuscr. Rep. Fish. Aquat. Sci. 3086: v + 20p. https://www.researchgate.net/publication/290194047_Climate_Change_Vulnerability_Assessment_Methodology_Workshop_Proceedings.*

•*Roux, M-J, Duplisea, DE, Karen Hunter, KL and Rice, J. 2022. Consistent risk management in a changing world: risk equivalence in fisheries and other human activities affecting marine resources and ecosystems. 	Frontiers in Climate. https://doi.org/10.3389/fclim.2021.781559*

![mackerel](images/charlotte-harrison-725lNk1BJec-unsplash.webp)
