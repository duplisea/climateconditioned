---
title: "Conditioning Advice"
weight: 3
header_menu: true
---

We call the process of accounting for climate and ecosystem changes conditioning advice. Fisheries advice is also regularly conditioned along the data and knowledge continuum using buffers in jurisdictions such as [Australia](https://www.agriculture.gov.au/sites/default/files/sitecollectiondocuments/fisheries/domestic/harvest-strategy-policy-guidelines.docx) and [ICES](https://www.ices.dk/sites/pub/Publication%20Reports/Advice/2021/2021/Advice_on_fishing_opportunities.pdf)
.


Conditioning factors used as buffers are used to change fisheries exploitation advice such that it remains sustainable when our ability to characterise uncertainty has changed. It is important to note that conditioning advice may not always lead to a reduction in fishing opportunities but can lead to advice for increased quotas and catches when climate or ecosystem change may enhance the production or distribution of a stock: that is, advice conditioning can lead to positive risk outcomes while buffering advice is always used as a penalty to compensate for negative risks. Conditioning advice according to principles of risk-equivalency is a means to balance sustainable exploitation and ecosystem change in an tractable and transparent way that is relatively easily communicated and implemented.

![tightrope](images/loic-leray-snbf9h8rjCw-unsplash.webp)

### Buffering advice for data availability
Buffering advice along the data and knowledge continuum is implemented as a penalty on allowable catch or fishing mortality when the approach used to assess stock status and sustainable exploitation is less certain than a more data rich method. The idea behind the buffering approach is that when data or knowledge used to inform the advice is limited, then the management decision based on that advice inherently entails more risk to the resource compared to a management decision in a data rich situation where uncertainties are better characterised. Therefore, in order to maintain risk equivalence in decision making, advice needs to be buffered or penalised to maintain a similar level of risk. In ICES, this risk-equivalent approach is called the "precautionary buffer" and is applied in particular prescribed situations outlined in the ICES advice guidelines.

The concept of applying risk-equivalent buffers can be used more broadly than along the data richness continuum but can be applied in any situation where data, a model, or changes in a marine ecosystem's capacity to withstand manageable human pressures changes. This includes when climate and ecosystem changes affect production processes.


### Conditioning advice to climate and ecosystem changes
As with buffering advice to data and knowledge availability, advice can be buffered to changing ecological and climatic conditions. So for instance, in the case of an abundance increase in the main prey species of a predator that is exploited by a fishery, the ecological conditioning would suggest an increase quota or fishing mortality could be exerted by the industry without taking on increased risk of stock collapse. The exact amount of increased catch would be determined by risk-equivalent conditioning. Likewise, if it has been hypothesised that climate change will lead to a decline in the production of an exploited fish stock over the advice period (say 10 years or less), then climate conditioning advice will lead to a recommendation of decreased quota or fishing mortality on a stock so as to maintain an equivalent chance of achieving stock objectives as before.

![market](images/e-9Ep9bbc5zVY-unsplash.webp)