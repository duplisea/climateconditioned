---
title: "About Us"
weight: 8
header_menu: false
---

Our work concentrates on providing science advice to inform decisions on managing human activities in the marine environment. Most of our work is in the area of fisheries and providing sustainable exploitation advice for stocks experiencing climate and ecosystem changes. These concept are also applicable in many areas where risk managment decisions are made based on science advice like marine protected area networks and fish habitat protection. Since 2017, we have been working on objectives based management and risk evaluations to inform managers of the consequences of different decisions that include climate and ecosystem change considerations.
![](images/ccr.webp)
**Daniel Duplisea** (Mont-Joli, Québec. *daniel.duplisea@dfo-mpo.gc.ca*) &nbsp;
**Karen Hunter** (Nanaimo, British Columbia. *karen.hunter@dfo-mpo.gc.ca*) &nbsp;
**Jake Rice** (Ottawa, Ontario. *jake.rice@dfo-mpo.gc.ca*) &nbsp;     
**Marie-Julie Roux** (Mont-Joli, Québec. *marie-julie.roux@dfo-mpo.gc.ca*) &nbsp;
&nbsp;

We work with scientists and managers in government, academia, inter-governmental organisations and non-governmental organisations in Canada and elsewhere. We are also cognisant of how our work must consider Indigenous peoples and the ongoing need for decolonialisation of science, management objectives, management practices and access to marine resources and we are attempting to do this practically not just aspirationally.

![](images/collage1.webp)

