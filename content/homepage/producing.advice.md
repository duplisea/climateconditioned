---
title: "Reviewing advice"
weight: 5
header_menu: false
---

Stock assessment advice in most jurisdictions requires evaluation by peer-review before it can be accepted for use. An office usually exists in most jurisdictions to do this such as the [Canadian Science Advisory Secretariat](https://www.dfo-mpo.gc.ca/csas-sccs/index-eng.htm). In most cases, incremental risk-based advice that considers climate change can be just part of the suite of advice coming from a peer-review process and is subject to its rigours. Because climate conditioned advice is incremental to the core unconditioned advice, even its rejection in peer review would still provide decision makers with core advice (unless that advice also is rejected). A climate conditioning process like what is described here enriches the advice to account for a wider set of issues without endangering by encumbering core advice in peer-review.

![fucus](images/noaa.plankton.webp)
