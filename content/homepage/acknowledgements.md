---
title: "Acknowledgements"
weight: 10
header_menu: false
---

Most of the photos here are openly licenced and are available from Unsplash:
[Nuno Antunes](https://unsplash.com/s/photos/nuno-antunes); [Silas Baisch](https://unsplash.com/s/photos/silas-baisch); [e](https://unsplash.com/s/photos/e); [Charlotte Harrison](https://unsplash.com/s/photos/charlotte-harrison); [L J](https://unsplash.com/s/photos/l-j); [Loic Leray](https://unsplash.com/s/photos/loic-leray); [Jamie Morrison](https://unsplash.com/s/photos/jamie-morrison); [Fer Nando](https://unsplash.com/s/photos/fer-nando); [NOAA](https://unsplash.com/@noaa); [Kateryna T](https://unsplash.com/s/photos/kateryna-t); [sour_cracker_photography](https://unsplash.com/@sour_cracker_photography); [USGS](https://unsplash.com/@usgs); [James Wheeler](https://unsplash.com/s/photos/james-wheeler). The Gulf Stream image is a screen grab from [Windy](https://windy.com). This site was built with Hugo and the Hugo-Scroll theme, thanks to [Jan Raasch](https://github.com/janraasch/hugo-scroll).