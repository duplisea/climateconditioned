---
title: "Tools"
weight: 4
header_menu: true
---

We advocate advice conditioning using a range or situation specific methods. It is important for one to situate the data and knowledge of the stock or system and the factors impacting it before adopting any particular approach. Therefore, like the buffering approach for data availability, methods for conditioning advice become clearer when one can place their question in this context. Before embarking on an advice conditioning process, it is important to visualise, study and communicate the kinds of climate or ecosystem changes a system is experiencing.

### Tools to visualise climate and ecosystem changes
In recent years, the use of climate stripes plots have been effective means of rapid visual communication of climate and ecosystem changes. Climate stripes are useful for this first pass and communicating visually with an audience with varying levels of expertise. For example, in fisheries advisory processes in Canada, there can be a very diverse audience present at the same meeting including quantitative researchers, social scientists, fishery managers, policy experts, fishing organisation representatives, First Nation representatives, ENGOs and individual fishers. While these processes remain science meetings, the presence of such a diverse participant group necessitates that science is well communicated in understandable ways to such a group for at least some parts of these meetings. Climate stripes plots are one tool which can aid communication showing the magnitude of changes experienced in ecosystems.


*Climate stripes*
![cs](images/colour.stripes.group.webp)

The climate stripes plot of the Hadley sea surface temperature data since 1850 clearly conveys the increase in temperature since the 1970s both through smooth data lines and colour bars. This kind of plot can provide a good supplement for communication of changes in an ecosystem to accompany more detailed quantitative analyses. Variations on climate stripes are possible such as rings, pies, radial plots and other graphical formats.

You can find an R-package to make climate stripes at https://github.com/duplisea/climatestripes



### Tools to condition advice with data-rich quantitative methods
This is the kind of conditioning that would occur when an ecological or environmental factor is built directly into the assessment model and the advice is therefore naturally conditioned to that factor. This could be considered the gold standard to make sure that advice considers factors such as climate change but it has its drawbacks. The main issue is that assessment models that include these external forcers may not have the mechanisms of action described in the same first principles detail as the internal population dynamics. In peer-review processes for developing advice from models, this can be problematic because it is incumbent upon the assessor to justify sometimes speculative mechanisms and if they cannot do so adequately, the whole assessment model may be rejected for use. Most assessors are not willing to die upon that hill and most managers are not willing to forego the possibility of having quantitative advice for the inclusion of what might be considered a speculative and poorly described mechanism forecasted over the long term.

An approach which is perhaps more palatable to assessors, managers and stakeholders is to do a *post hoc* conditioning of advice that comes from the data-rich quantitative model based on an ecological or environmental variable (E). This preserves both the original model advice that does not consider E and an added piece of advice with E conditioning. Doing conditioning in this way ensures that there is always advice to fall back upon even in the case where the E mechanism is considered too speculative and rejected. Empirical modelling is one way to do this *post hoc* conditioning. [Researchers](https://www.thelifeaquatic.xyz/about) at Memorial University of Newfoundland and Labrador have taken up the challenge of this kind of conditioning in fisheries assessments.

### Tools to condition advice using data-moderate empirical models
Empirical models describe how a stock's productivity state changes with a change in E through the use of empirically derived relationships. The advantage of empirical modelling approaches is that they utilise the larger scale phenomenon empirically observed to link E to the future state of a stock and they are applicable to a wider variety of marine management situations than just stock assessment. Empirical models can be used to condition advice to E that comes from a data-rich modelling approach or they can be part of simpler modelling approaches. Empirical models should have a plausible hypothesis on how the E variable affects a stock or system in order to be falsifiable but they can equally support more speculative processes. This intermediate modelling approach is flexible and powerful and, because they do not form the core of the assessment approach themselves, conditioning advice with empirical modelling is an added piece of advice that does not invalidate the whole assessment process if the conditioned advice is not used or rejected. Finally, empirical modelling approaches can usually be developed and run within a short time period which can ensure the timely delivery of conditioned advice to managers.

We advocate the use of empirical modelling approaches to delineate a risk-equivalent safe operating space for fisheries exploitation when environmental or ecological factors are causing significant shifts in stock productivity dynamics. Such safe operating spaces could be defined for other kinds of human activities in marine ecosystems as well.
![SOS](images/safe.operating.space.webp)

A specific application of an empirical model approach to condition fisheries exploitation advice to climate change is available as an R-package at https://github.com/duplisea/ccca


### Tools to condition advice using data-limited scoring based methods
Scoring based methods can be used to condition advice in situations where there is limited data and or knowledge for how to assess a stock. These methods rely on the statement of a direction hypothesis about the impact of E on stock dynamics and they need to force the conditioning through expert elicitation, literature and experimental inference. Because there is usually not enough or even any hard quantitative information, scoring categorical approaches can be amongst the best ways to reflect different degrees of conditioning. Though these approaches are inspired by and consistent with ideas of risk-equivalency, they can usually only infer that a risk equivalent advice conditioning is being implemented rather than being certain of it. A promising way to further inform a scoring approach and bring it into closer alignment with a risk-equivalent advice adjustment is to test the scoring in simulations with empirical or data-rich models.

Risk conditioning can be implemented as an environmental conditioning factor (ECF) that multiplies the unconditioned advice. The conditioning factor needs to have certain mathematical properties such as the conditioning factor should be 1 when the environmental conditions are at baseline values. The exact shape of the conditioning factor vs E curve can be linear or non-linear and this will be a function of how a population reacts to changes in the environmental or ecological conditions.

![ECFlog](images/scoring.combined.svg)

Scoring based conditioning methods are currently under development and will be available as an R-package on github when they are ready.

![fucus](images/fucus.webp)
