sf= function(x, minx, maxx){
  x.scaled= minx+((maxx-minx)*(x-min(x)))/(max(x)-min(x))
  x.scaled
}

inv= function(x, y){
  # calculates the symmetrical inverse value by computing the difference of the curve from a straight line
  # then adding it to the straight line prediction
  m= (y[1]-tail(y,1))/(x[1]-tail(x,1))
  b= 1
  yline= x*m+b
  yresidual= yline-y
  y2= yline+yresidual
  y2
}

svg("~/github/ccr3/static/images/scoring.combined.svg",bg="transparent")
par(mfcol=c(2,1), mar=c(2,2,0,2), omi=c(.8,.8,.8,.8))
a= 2
SEsamp= 1.2
SFsamp= 1
Cmax=SEsamp*SFsamp
Ezbase= 0
Ez.scenario= seq(-4,4,length=100)
C= SEsamp^((Ez.scenario-Ezbase)*SFsamp)
Cneg= (1/SEsamp)^((Ez.scenario-Ezbase)*SFsamp)
matplot(Ez.scenario, cbind(C,Cneg), lwd=4,type="l", lty=1,col="grey", xlab="",ylab="",xaxt='n',
        col.axis = "white")
abline(h=1,col="grey")
abline(v=Ezbase,col="grey")
#legend("top",legend=paste("Ebase=",Ezbase),bty="n",col="white")
box(col="white", lwd=3)

# logistic formulation
a=2
b=2
Ez.scenario= seq(-4,4,length=100)
C= a/(1+exp(-b*(Ez.scenario-Ezbase)))
Cneg= a/(1+exp(b*(Ez.scenario-Ezbase)))
#svg("~/github/ccr/static/images/logistic.scoring.svg")
matplot(Ez.scenario, cbind(C,Cneg), lwd=4,type="l", lty=1,col="grey", xlab="",ylab="",col.axis = "white")
abline(h=1,col="grey")
abline(v=Ezbase,col="grey")
#legend("top",legend=paste("Ebase=",Ezbase),bty="n")
box(col="white",lwd=3)
mtext(side=1,"E",outer=T,line=1.5,cex=1.5,col="white")
mtext(side=2,"ECF",outer=T,line=1.5,cex=1.5,col="white")
dev.off()
